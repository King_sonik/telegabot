package domain.models;

import java.sql.Date;
import java.util.List;

public class Creator extends User {
    private List<Income> income;
    private List<Item> item;

    public Creator(List<Income> income, List<Item> item) {
        this.income = income;
        this.item = item;
    }

    public Creator(String name, String surname, String username, String password, Date birthday, String role, List<Income> income, List<Item> item) {
        super(name, surname, username, password, birthday, role);
        this.income = income;
        this.item = item;
    }

    public Creator(long id, String name, String surname, String username, String password, Date birthday, String role, List<Income> income, List<Item> item) {
        super(id, name, surname, username, password, birthday, role);
        this.income = income;
        this.item = item;
    }

    public Creator(long id, String name, String surname, String username, Date birthday, String role, List<Income> income, List<Item> item) {
        super(id, name, surname, username, birthday, role);
        this.income = income;
        this.item = item;
    }

    public List<Income> getIncome() {
        return income;
    }

    public void setIncome(List<Income> income) {
        this.income = income;
    }

    public List<Item> getItem() {
        return item;
    }

    public void setItem(List<Item> item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return super.toString()+income + ", item=" + item;
    }
}
