package domain.models;

import java.sql.Date;
import java.util.List;

public class Gamer extends User{
    private List<Outcome> outcomes;

    public Gamer(List<Outcome> outcomes) {
        this.outcomes = outcomes;
    }

    public Gamer(String name, String surname, String username, String password, Date birthday, String role, List<Outcome> outcomes) {
        super(name, surname, username, password, birthday, role);
        this.outcomes = outcomes;
    }

    public Gamer(long id, String name, String surname, String username, String password, Date birthday, String role, List<Outcome> outcomes) {
        super(id, name, surname, username, password, birthday, role);
        this.outcomes = outcomes;
    }

    public Gamer(long id, String name, String surname, String username, Date birthday, String role, List<Outcome> outcomes) {
        super(id, name, surname, username, birthday, role);
        this.outcomes = outcomes;
    }

    public List<Outcome> getOutcomes() {
        return outcomes;
    }

    public void setOutcomes(List<Outcome> outcomes) {
        this.outcomes = outcomes;
    }

    @Override
    public String toString() {
        return super.toString() + outcomes ;
    }
}
