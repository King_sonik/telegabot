package domain.models;

public class Income extends Outcome {
    private User user;

    public Income() {
    }

    public Income(long id, Item item, int date, User user) {
        super(id, item, date);
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "This user:" + user +super.toString();
    }
}
