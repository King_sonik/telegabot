package domain.models;

public class Hero {
    private long id;
    private String name;
    private String photo;
    private String history;
    private String f1;
    private String f2;
    private String f3;
    private String f4;

    public Hero() {
    }

    public Hero(long id, String name, String photo, String history, String f1, String f2, String f3, String f4) {
        this.id = id;
        this.name = name;
        this.photo = photo;
        this.history = history;
        this.f1 = f1;
        this.f2 = f2;
        this.f3 = f3;
        this.f4 = f4;
    }

    public Hero(long id, String name, String photo, String history) {
        this.id = id;
        this.name = name;
        this.photo = photo;
        this.history = history;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public String getF1() {
        return f1;
    }

    public void setF1(String f1) {
        this.f1 = f1;
    }

    public String getF2() {
        return f2;
    }

    public void setF2(String f2) {
        this.f2 = f2;
    }

    public String getF3() {
        return f3;
    }

    public void setF3(String f3) {
        this.f3 = f3;
    }

    public String getF4() {
        return f4;
    }

    public void setF4(String f4) {
        this.f4 = f4;
    }

    @Override
    public String toString() {
        return "Hero{" +
                ", name='" + name + '\'' +
                ", photo='" + photo + '\'' +
                ", history='" + history + '\'' +
                ", f1='" + f1 + '\'' +
                ", f2='" + f2 + '\'' +
                ", f3='" + f3 + '\'' +
                ", f4='" + f4 + '\'' +
                '}';
    }
}
